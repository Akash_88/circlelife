
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class AndroidFBHomePage extends BaseDriver {

    public static void searchCirclesLife(){
        waitForVisibilityOf(By.xpath("//android.widget.EditText[@text()='Search']"));
        driver1.findElement(By.xpath("//android.widget.EditText[@text()='Search']")).sendKeys("Circles.Life");
        driver1.findElement(By.xpath("//android.widget.EditText[@text()='Search']")).sendKeys(Keys.ENTER);
    }
}
