import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class HomePage extends BaseDriver {

    private static String signUp="//div[@Classname='dn db-l']/a";
    public static void SignUpBtn(){
        try {
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//*[text()='Sign up']")).isDisplayed();
            driver.findElement(By.xpath("//*[text()='Sign up']")).click();
        }catch(Exception e){
            //Do nothing
        }
    }
}
