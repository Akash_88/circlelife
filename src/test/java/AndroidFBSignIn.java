
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AndroidFBSignIn extends BaseDriver {

    public static void FBSignIn(String userid ,String pass){
        waitForVisibilityOf(By.xpath("//android.widget.EditText[@text()='Email or Phone']"));
        driver1.findElement(By.xpath("//android.widget.EditText[@text()='Email or Phone']")).clear();
        driver1.findElement(By.xpath("//android.widget.EditText[@index='0']")).sendKeys(userid);
        driver1.findElement(By.xpath("//android.widget.EditText[@text()='Password']")).sendKeys(pass);
        driver1.findElement(By.xpath("//android.widget.Button[@text()='LOG IN']")).click();
    }
}
