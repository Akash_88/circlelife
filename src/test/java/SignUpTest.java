import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.security.Key;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class SignUpTest extends BaseDriver {

    @Test
    public void SignUpTest(){
        launchDriver().get("https://pages.circles.life/");
        Random rand = new Random();
        int randvalue = rand.nextInt(1000);
        String email = "text"+randvalue+"@test.com";
        System.out.println("Email id"+email);
        HomePage.SignUpBtn();
        SignUpPage.firstName("text");
        SignUpPage.lastName("sample");
        SignUpPage.emailId(email);
        SignUpPage.password("text12345");
        SignUpPage.submit();
        PlanPage.clickPlan();
        Assert.assertEquals(email,MyAccount.getEmailId());
    }
    @Test
    public void FacebookTest(){
        //launchDriver().get("https://www.facebook.com/");
        //FBHomePage.FBSignUp("testcircleslife123@gmail.com","Akash1234");
        //driver.findElement(By.cssSelector("body")).sendKeys(Keys.COMMAND +"t");
        launchDriver().get("https://pages.circles.life/");
        HomePage.SignUpBtn();
        String winHandleBefore = driver.getWindowHandle();
        SignUpPage.fbButtonClick();
        // Switch to new window opened
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
        FBHomePage.FBSignUp("testcircleslife123@gmail.com","Akash1234");
        driver.switchTo().window(winHandleBefore);
        PlanPage.fbClick();
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }

        FBCirclePages.addComment();
    }

    @Test
    public void appiumFBTest(){
        appiumDriver();
        AndroidFBSignIn.FBSignIn("testcircleslife123@gmail.com","Akash1234");
        AndroidFBHomePage.searchCirclesLife();
        AndroidFBCirlesLife.posterClick();
        AndroidFBCirlesLife.commentClick();
        Assert.assertEquals("Sampletest Sample",AndroidFBCirlesLife.getUserName());
    }

    @AfterMethod
    public void TearDown(){
        //driver.close();
    }
    public void dialogAccept() throws InterruptedException {
        Thread.sleep(3000);
        TouchAction act = new TouchAction(driver1);
        act.tap(691,1048).perform();
    }

}
