import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class FBHomePage extends BaseDriver {

    public static void FBSignUp(String email, String pass){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.id("email")).sendKeys(email);
        driver.findElement(By.id("pass")).sendKeys(pass);
        driver.findElement(By.id("u_0_0")).click();

    }
}
