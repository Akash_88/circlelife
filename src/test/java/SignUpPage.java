import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class SignUpPage extends BaseDriver {

    public static void firstName(String first){
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.findElement(By.name("first_name")).sendKeys(first);
    }
    public static void lastName(String last){
        driver.findElement(By.name("last_name")).sendKeys(last);

    }
    public static void emailId(String email){
        driver.findElement(By.name("email")).sendKeys(email);

    }
    public static void password(String pass){
        driver.findElement(By.name("password")).sendKeys(pass);

    }

    public static void submit(){
        driver.findElement(By.xpath("//*[text()='Create account']")).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void fbButtonClick(){
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//button[@class='btn btn-primary btn-lg btn-block facebook-login-button metro']")).click();
    }
}
