import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class FBCirclePages extends BaseDriver {

    public static void addComment(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Actions builder = new Actions(driver);
        builder.moveToElement(driver.findElement(By.xpath("//img[@src='https://scontent.fblr1-3.fna.fbcdn.net/v/t31.0-8/29352167_2135006993452234_3171374289983827588_o.jpg?_nc_cat=0&oh=9a813e886420024581d475ab37c0b5a4&oe=5BB163E1']")),10, 25).click().build().perform();
        driver.findElement(By.id("addComment_2135006993452234")).click();
        driver.switchTo().activeElement().sendKeys("Hello,  Circles life");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.switchTo().activeElement().sendKeys(Keys.RETURN);

    }
}
