import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class BaseDriver {
    static WebDriver driver;
    static AndroidDriver driver1;
    static DesiredCapabilities capabilities;
    public WebDriver launchDriver() {
        System.setProperty("webdriver.gecko.driver","/Users/akashgoswami/IdeaProjects/circleslife/src/main/resources/geckodriver");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        //driver.manage().window().fullscreen();
        return driver;
    }

    public WebDriver appiumDriver(){
        //File appDir = new File("/Users/akashgoswami/IdeaProjects/circleslife/src/main/resources/");
        //File app = new File(appDir, "com.facebook.katana_v176.0.0.42.87-113613973_Android-4.0.3.apk");
        //System.out.println("app Dir.--->"+app);

        capabilities = new DesiredCapabilities();

        // Name of mobile web browser to automate. It should be an empty string, as we are automation an app
        //capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        // Name of the OS: Android, iOS or FirefoxOS
        //capabilities.setCapability("platformName", "Android");
        // Device name:  – I am using Galaxy
        //capabilities.setCapability("deviceName", "ONEPLUS A5000");
        // Mobile OS version –  My device is running Android 4.4.4
        //capabilities.setCapability("platformVersion", "8.0");
        // An absolute local path to the APK file
        //capabilities.setCapability("app", app.getCanonicalPath());

        // Java package of the tested Android app
        //capabilities.setCapability("appPackage", "com.facebook.katana");

        // An activity name for the Android activity you want to run from your package.
        //capabilities.setCapability("appActivity", "com.facebook.katana.platform.FacebookAuthenticationActivity");

//       Constructor to initialize driver object with new Url and Capabilities




        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "8.0");
        capabilities.setCapability("deviceName","ONEPLUS A5000");
        capabilities.setCapability("app", "/Users/akashgoswami/IdeaProjects/circleslife/src/main/resources/com.facebook.katana.apk");
        capabilities.setCapability("appPackage", "com.facebook.katana");
        capabilities.setCapability("appActivity", "com.facebook.katana.platform.FacebookAuthenticationActivity");
        //capabilities.setCapability("appWaitActivity", "com.facebook.katana.dbl.activity.FacebookLoginActivity");
        //capabilities.setCapability("autoAcceptAlerts", true);

        try {
            driver1 = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver1.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver1;
    }
    protected static void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForVisibilityOf1(By locator) {
        WebDriverWait wait = new WebDriverWait(driver1, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    protected void waitForClickabilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void scrollPageUp() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.50);
        swipeObject.put("startY", 0.95);
        swipeObject.put("endX", 0.50);
        swipeObject.put("endY", 0.01);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }


    public void swipeLeftToRight() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.01);
        swipeObject.put("startY", 0.5);
        swipeObject.put("endX", 0.9);
        swipeObject.put("endY", 0.6);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }

    public void swipeRightToLeft() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.9);
        swipeObject.put("startY", 0.5);
        swipeObject.put("endX", 0.01);
        swipeObject.put("endY", 0.5);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }

    public void swipeFirstCarouselFromRightToLeft() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.9);
        swipeObject.put("startY", 0.2);
        swipeObject.put("endX", 0.01);
        swipeObject.put("endY", 0.2);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }

    public void performTapAction(WebElement elementToTap) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, Double> tapObject = new HashMap<String, Double>();
        tapObject.put("x", (double) 360); // in pixels from left
        tapObject.put("y", (double) 170); // in pixels from top
        tapObject.put("element", Double.valueOf(((RemoteWebElement) elementToTap).getId()));
        js.executeScript("mobile: tap", tapObject);
    }

}
