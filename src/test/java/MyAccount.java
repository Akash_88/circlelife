import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class MyAccount extends BaseDriver {


    public static String getEmailId(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement element = driver.findElement(By.xpath("(//div[@class='form-group row'])[7]"));
        System.out.println("Value::::::"+element.findElement(By.className("form-control")).getAttribute("value").toString());
        return element.findElement(By.className("form-control")).getAttribute("value").toString();

    }

}
