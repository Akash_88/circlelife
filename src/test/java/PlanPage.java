import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class PlanPage extends BaseDriver {

    public static void clickPlan(){
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[text()='MY ACCOUNT']")).isDisplayed();
        driver.findElement(By.xpath("//*[text()='MY ACCOUNT']")).click();
    }

    public static void fbClick(){
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//img[@src='/assets/_react/bundle/images/49eea9c9cde76554c504d61caeb4d2ef.svg']")).click();
    }

}
