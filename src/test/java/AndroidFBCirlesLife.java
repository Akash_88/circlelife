
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class AndroidFBCirlesLife extends BaseDriver {

    public static void posterClick(){
        waitForVisibilityOf(By.xpath("//android.view.ViewGroup[@index='0']"));
        driver1.findElement(By.xpath("//android.view.ViewGroup[@index='0']")).click();
    }

    public static void commentClick(){
        waitForVisibilityOf(By.xpath("//android.widget.TextView[@content-desc='Comment Button']"));
        driver1.findElement(By.xpath("//android.widget.TextView[@content-desc='Comment Button']")).click();
    }

    public static String getUserName(){
        waitForVisibilityOf(By.xpath("//android.view.ViewGroup[@index='0']"));
        return driver1.findElement(By.xpath("//android.view.ViewGroup[@index='0']")).getText();
    }

}
